var myApp = angular.module('myApp', ['chart.js'])


myApp.filter('date', function(){
	return function(input){
		return input.substring(0, input.indexOf("T"))
	}
})

myApp.filter('dateTime', function(){
	return function(input){
		dateTime = input.split("T")
		return dateTime[0] + " " + dateTime[1].substring(0, dateTime[1].indexOf(4,":") + 2)
	}
})


myApp.filter('datesecond’, function(){
	return function(input){
		dateTime = input.split("T")
		return dateTime[0] + " " + dateTime[1].substring(0, dateTime[1].indexOf(4,":") + 2)
	}
})


myApp.factory('gitProvider', function($q, $http){

	gitSearch = function(repo){

		var defer = $q.defer()

		$http.get('https://api.github.com/search/repositories?q=' + repo).success(
			function(data){
				defer.resolve(data)
			}).error(function(error){
				defer.reject(error)
			})

		return defer.promise
	}

	getGitIssues = function(userName, repoName){
		
		var defer = $q.defer()

		$http.get('https://api.github.com/search/issues?q=repo:' + userName +'/' +repoName).success(
			function(data){
				defer.resolve(data)
			}).error(function(error){
				defer.reject(error)
			})

		return defer.promise
	}

	getIssueComments = function(url){
		var defer = $q.defer()

		$http.get(url).success(
			function(data){
				defer.resolve(data)
			}).error(function(error){
				defer.reject(error)
			})

		return defer.promise
	}

	return {
		gitSearch: gitSearch,
		getGitIssues: getGitIssues,
		getIssueComments: getIssueComments
	}

})

myApp.controller('searchCtrl', function($scope, gitProvider) {

	$scope.input = ""
	$scope.searching = false
	$scope.error = ""
	$scope.displayData = ""
	$scope.tab = "info"


	$scope.labels = ["Forks count", "Stargazer count", "Open issues count"];
  	// $scope.data = [300, 500, 100];

	$scope.setTab = function(tab){
		$scope.tab = tab
	}

	$scope.search = function(){
		$scope.searching = true
		$scope.setTab("info")
		$scope.displayData = ""
		gitProvider.gitSearch($scope.input).then(function(data){
			$scope.searching = false
			$scope.result = data
		}).catch(function(error){
			$scope.searching = false
			$scope.error = error
		})
	}

	$scope.showData = function(repo){
		$scope.setTab("info")
		$scope.displayData = repo;
	}

});

myApp.directive('gitIssue', function(gitProvider){
	return {
		retrict: 'E',
		scope:{
			displayData : "="
		},
		templateUrl: 'git-issues-template.html',
		link: function($scope){

			$scope.displayIssue = ""
			$scope.comments = ""
			$scope.error = ""
			$scope.loading = false

			$scope.showIssue = function(issue){
				$scope.displayIssue = issue;
				$scope.issueComments(issue)
			}

			$scope.issues = function(repo){
				$scope.loading = true
				gitProvider.getGitIssues(repo.owner.login, repo.name).then(function(data){
					$scope.loading = false
					repo.issues = data
				}).catch(function(error){
					$scope.loading = false
					$scope.error = error
				})
			}

			$scope.issueComments = function(issue){
				gitProvider.getIssueComments(issue.comments_url).then(function(data){
					$scope.comments = data
				}).catch(function(error){
					$scope.error = error
				})
			}

			$scope.issues($scope.displayData)
		}
	}
});

myApp.directive('statisticPage', function(){
	return {
		retrict: 'E',
		scope:{
			displayData : "="
		},
		templateUrl: 'chart-template.html',
		link: function($scope){
			$scope.labels = ['forks', 'stargazers', 'open issues']
			$scope.data = [$scope.displayData.forks_count, $scope.displayData.stargazers_count, $scope.displayData.open_issues_count]
		}
	}
})